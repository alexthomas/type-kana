package com.alexk8s.typekana;

import com.alexk8s.typekana.model.Result;
import com.alexk8s.typekana.model.Session;
import com.alexk8s.typekana.repository.ResultRepository;
import com.alexk8s.typekana.repository.SessionRepository;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;

@Component
@Slf4j
@AllArgsConstructor
public class CliApp implements CommandLineRunner {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";


    private final ResultRepository resultRepository;
    private final SessionRepository sessionRepository;
    private final WebClient highsubWebClient = WebClient.builder().baseUrl("https://highsub.alexk8s.com").build();
    private final List<String> words = new ArrayList<>();

    private enum Mode {
        HIRAGANA,
        KATAKANA
    }

    public record ReadingBreakdown(String katakana, String hiragana, String romaji,
                                   List<ReadingBreakdownToken> tokens) {
    }

    public record ReadingBreakdownToken(String katakana, String hiragana, String romaji) {
    }

    private final AtomicReference<Mode> mode = new AtomicReference<>();

    private final Scanner scanner = new Scanner(System.in);

    @PostConstruct
    public void loadWords() {
        Resource netflixFile = new ClassPathResource("type-kana/Netflix10K.txt");
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(netflixFile.getInputStream()))) {
            reader.lines().forEach(words::add);
        } catch (IOException i) {
            log.error("Error reading file", i);
        }
    }

    @Override
    public void run(String... args) throws Exception {
        printDailyStatus();
        System.out.println();
        configureMode();
        Session session = sessionRepository.save(new Session(null, System.currentTimeMillis(), mode.get().toString(), null)).block();
        while (true) {
            String word = nextWord();
            ReadingBreakdown reading = getReading(word).block();
            if (reading == null)
                break;
            printWord(reading);
            long startUtc = System.currentTimeMillis();
            String input = scanner.next();
            if ("q".equals(input)) {
                break;
            }
            ResponseEvaluation responseEvaluation = evaluateResponse(input, reading);
            Result result = Result.builder().word(word).reading(reading.katakana).hiragana(reading.hiragana).sessionId(session.getId())
                    .startUtc(startUtc)
                    .endUtc(System.currentTimeMillis()).enteredCorrectly(responseEvaluation.correct()).enteredRomaji(input).build();
            resultRepository.save(result).block();
            System.out.print(responseEvaluation.formattedRomajiOutput +  "   " +responseEvaluation.formattedKanaOutput+"   ");
            printDailyStatus();
            System.out.println();

        }
    }

    private record ResponseEvaluation(boolean correct, String formattedRomajiOutput, String formattedKanaOutput) {
    }

    private ResponseEvaluation evaluateResponse(String input, ReadingBreakdown reading) {
        int cursor = 0;
        boolean previousCorrect = true;
        boolean isCorrect = true;
        StringBuilder formattedRomajiOutput = new StringBuilder();
        StringBuilder formattedKanaOutput = new StringBuilder();
        for (ReadingBreakdownToken token : reading.tokens) {
            if (!previousCorrect) {
                int nextStart = input.indexOf(token.romaji, cursor);
                if (nextStart != -1)
                    cursor = nextStart;
            }
            String correctCharacter = mode.get() == Mode.HIRAGANA ? token.hiragana : token.katakana;
            if (checkRomaji(input, token.romaji, cursor)) {
                formattedKanaOutput.append(ANSI_GREEN).append(correctCharacter).append(ANSI_RESET);
                formattedRomajiOutput.append(ANSI_GREEN).append(token.romaji).append(ANSI_RESET);
                cursor += token.romaji.length();
                previousCorrect = true;
            } else {
                formattedKanaOutput.append(ANSI_RED).append(correctCharacter).append(ANSI_RESET);
                formattedRomajiOutput.append(ANSI_RED).append(token.romaji).append(ANSI_RESET);
                previousCorrect = false;
                isCorrect = false;
            }
        }
        return new ResponseEvaluation(isCorrect, formattedRomajiOutput.toString(), formattedKanaOutput.toString());
    }

    private boolean checkRomaji(String input, String expectedRomaji, int cursor) {
        if ("n'".equals(expectedRomaji)) {
            return input.startsWith("nn", cursor) || input.startsWith("n'", cursor);
        }
        return input.startsWith(expectedRomaji, cursor);
    }

    private void printDailyStatus() {

        long epochSecond = LocalDate.now().atStartOfDay(ZoneOffset.systemDefault()).toEpochSecond();
        Long dailyCount = resultRepository.findByEndUtcGreaterThan(epochSecond * 1000).count().block();
        System.out.print(" Daily count: " + dailyCount);
    }

    private String nextWord() {
        return words.get((int) (Math.random() * words.size()));
    }

    private void configureMode() {
        while (mode.get() == null) {
            System.out.println(ANSI_GREEN + "press 'h' for hiragana, 'k' for katakana" + ANSI_RESET);
            String input = scanner.nextLine().toLowerCase();
            if ("h".equals(input)) {
                mode.set(Mode.HIRAGANA);
            } else if ("k".equals(input)) {
                mode.set(Mode.KATAKANA);
            } else {
                System.out.println(ANSI_RED + "invalid input" + ANSI_RESET);
            }
        }
    }

    private void printWord(ReadingBreakdown reading) {
        if (mode.get() == Mode.HIRAGANA) {
            System.out.println(reading.hiragana);
        } else {
            System.out.println(reading.katakana);
        }
    }

    private Mono<ReadingBreakdown> getReading(String word) {
        return highsubWebClient.get()
                .uri("/v1/japanese/" + word + "/reading-breakdown")
                .retrieve()
                .bodyToMono(ReadingBreakdown.class);
    }


}
