package com.alexk8s.typekana.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Data
@AllArgsConstructor
@Table("sessions")
public class Session {
    @Id
    private UUID id;
    @Column("created_utc")
    private Long createdUtc;
    private String mode;
    @Version
    private Long version;
}
