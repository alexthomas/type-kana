package com.alexk8s.typekana.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Table("results")
@Data
@AllArgsConstructor
@Builder
public class Result {
    @Id
    private UUID id;
    private String word;
    private String reading;
    private String hiragana;
    @Column("start_utc")
    private long startUtc;
    @Column("end_utc")
    private long endUtc;
    @Column("entered_correctly")
    private boolean enteredCorrectly;
    @Column("entered_romaji")
    private String enteredRomaji;
    @Version
    private Long version;
    @Column("session_id")
    private UUID sessionId;

}
