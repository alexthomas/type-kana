package com.alexk8s.typekana;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TypeKanaApplication {

    public static void main(String[] args) {
        SpringApplication.run(TypeKanaApplication.class, args);
    }

}
