package com.alexk8s.typekana.repository;

import com.alexk8s.typekana.model.Session;
import org.springframework.data.r2dbc.repository.R2dbcRepository;

import java.util.UUID;

public interface SessionRepository extends R2dbcRepository<Session, UUID> {
}
