package com.alexk8s.typekana.repository;

import com.alexk8s.typekana.model.Result;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;

import java.util.UUID;

public interface ResultRepository extends R2dbcRepository<Result, UUID> {
    Flux<Result> findByEndUtcGreaterThan(long endUtc);
}
